<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Looping</title>
</head>
<body>
	<h1>Berlatih Looping</h1>
	<?php 
	echo "Soal 1 <br>";
	echo "Looping Pertama <br>";
	for($i=0; $i<=20; $i+=2){
		echo $i . "I Love PHP <br>";
	}
	echo "Looping Kedua <br>";
	for ($i=20; $i>=2; $i-=2){ 
		echo $i. "I Love PHP <br>";
	}

	echo "<h3>Soal No 2 Looping Array Modulo </h3>";
        /* 
            Soal No 2
            Looping Array Module
            Carilah sisa bagi dengan angka 5 dari setiap angka pada array berikut.
            Tampung ke dalam array baru bernama $rest 
        */

        $numbers = [18, 45, 29, 61, 47, 34];
        echo "array numbers: <br>";
        print_r($numbers);
        // Lakukan Looping di sini
        foreach($numbers as $value){
        	$rest[]=$value %= 5;
        }

        echo "<br>";
        echo "Array sisa baginya adalah:  "; 
        echo "<br>";
        print_r($rest);
        echo "<br>";

         echo "<h3> Soal No 3 Looping Asociative Array </h3>";
         $items = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];

        foreach($items as $key => $value){
        	$item = array(
        		'id' => $value[0],
        		'name' => $value[1],
        		'price' => $value[2],
        		'description' => $value[3],
        		'source' => $value[4]
        	);
        	
        	print_r($item);
        	echo "<br>";
        }


        echo "<h3>Soal No 4 Asterix </h3>";
        /* 
            Soal No 4
            Asterix 5x5
            Tampilkan dengan looping dan echo agar menghasilkan kumpulan bintang dengan pola seperti berikut: 
            Output: 
            * 
            * * 
            * * * 
            * * * * 
            * * * * *
        */
        echo "Asterix: ";
        echo "<br>";

        for($u=1;$u<=5;$u++){
        	for($d=1;$d<=$u;$d++){
        		echo "*";
        	}
        	echo "<br>";
        }        

	 ?>

</body>
</html>